#------------------------------------------
#--- Author: Pradeep Singh
#--- Date: 20th January 2017
#--- Version: 1.0
#--- Python Ver: 2.7
#--- Details At: https://iotbytes.wordpress.com/store-mqtt-data-from-sensors-into-sql-database/
#------------------------------------------

#obsluhuji tyto senzory:
#seznam:
#Home/balcony/Humidity
#

import json
import pymysql.cursors

# Function return a connection.
def getConnection():
  import pymysql.cursors
  # You can change the connection arguments.
  try:
    connection = pymysql.connect(host='localhost',
                                 user='mqtt',
                                 password='m1Axc1Eu1GUlXDQX',
                                 db='mqtt',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
                                     
                                     
  except:
    return None

  return connection

#===============================================================
# Master Function to Select DB Funtion based on MQTT Topic
def sensor_Data_Handler(Topic, jsonData):
  if Topic == "Home/balcony/Humidity":
    #DHT22_Temp_Data_Handler(jsonData)
    Humidity_Data_Handler(jsonData)
  #elif Topic == "Home/BedRoom/DHT22/Humidity":
  #  Humidity_Data_Handler(jsonData)

#===============================================================

# Function to save Temperature to DB Table
def DHT22_Temp_Data_Handler(jsonData):
  #Parse Data
  json_Dict = json.loads(jsonData)
  SensorID = json_Dict['Sensor_ID']
  #Data_and_Time = json_Dict['Date']
  Temperature = json_Dict['Temperature']
  connection = getConnection()
  
  if connection is None: 
    return None
  
  try:
    
  
    cursor = connection.cursor()
    # Create a new record
    
    print ("MQTT Data Received...")
    print ("SensorID:")
    print (SensorID)
    #print ("Data_and_Time: ")
    #print (Data_and_Time)
    print ("Temperature: ")
    print (Temperature)
    print (".")
    print (".")
    print (".")
    print (".")
          
    sql = 'INSERT INTO SensorTemperature(id, SensorId,SensorValue) VALUES (NULL, %s,%s);'
    cursor.execute(sql, (SensorID,Temperature))
    
    #sql = "INSERT INTO SensorTemperatureData (id, SensorId,SensorDatetime, SensorValue) VALUES (NULL,'TESTSS', '2018-06-12 13:09:01', '85.5');"
    #cursor.execute(sql)
  
    # connection is not autocommit by default. So you must commit to save
    # your changes.
  except (MySQLdb.Error, MySQLdb.Warning) as e:
    print(e)
    return None
  finally:
    connection.commit()


# Function to save Humidity to DB Table
def Humidity_Data_Handler(jsonData):
  #Parse Data
  json_Dict = json.loads(jsonData)
  SensorID = json_Dict['Sensor_ID']
  #Data_and_Time = json_Dict['Date']
  Temperature = json_Dict['Humidity']
  connection = getConnection()
  
  if connection is None: 
    return None
  
  try:
    
  
    cursor = connection.cursor()
    # Create a new record
    
    print ("MQTT Data Received...")
    print ("SensorID:")
    print (SensorID)
    #print ("Data_and_Time: ")
    #print (Data_and_Time)
    print ("Temperature: ")
    print (Temperature)
    print (".")
    print (".")
    print (".")
    print (".")
          
    sql = 'INSERT INTO SensorHumidity(id, SensorId,SensorValue) VALUES (NULL, %s,%s);'
    cursor.execute(sql, (SensorID,Temperature))
    
    #sql = "INSERT INTO SensorTemperatureData (id, SensorId,SensorDatetime, SensorValue) VALUES (NULL,'TESTSS', '2018-06-12 13:09:01', '85.5');"
    #cursor.execute(sql)
  
    # connection is not autocommit by default. So you must commit to save
    # your changes.
  except (MySQLdb.Error, MySQLdb.Warning) as e:
    print(e)
    return None
  finally:
    connection.commit()