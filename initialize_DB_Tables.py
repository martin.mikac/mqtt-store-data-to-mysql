# -*- coding: utf-8 -*
#------------------------------------------
#--- Author: Martin Mikac
#--- Date: 12.06.2018
#--- Version: 1.0
#------------------------------------------


import pymysql.cursors

# Connect to the database
connection = pymysql.connect(host='localhost',
                             user='mqtt',
                             password='m1Axc1Eu1GUlXDQX',
                             db='mqtt',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

try:
    with connection.cursor() as cursor:
        # Create a new record

        sql = "INSERT INTO `SensorTemperatureData` (`id`, `SensorId`, `SensorDatetime`, `SensorValue`) VALUES (NULL, 'test', '2018-06-12 01:12:34', 'testovací SEnzor');"

        #sql = "INSERT INTO `users` (`email`, `password`) VALUES (%s, %s)"

        ##cursor.execute(sql, ('webmaster@python.org', 'very-secret'))
        cursor.execute(sql)

    # connection is not autocommit by default. So you must commit to save
    # your changes.
    connection.commit()

finally:
    connection.close()
